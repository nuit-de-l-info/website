<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Riddle5Controller extends Controller
{
    public function diplay()
    {
        return view('riddles/riddle5');
    }

    public function answer(Request $request)
    {
        $request->validate([
            'answer' => 'required',
        ]);

        if ($request->input('answer') == 'monstre') {
            session(['answer5' => '1']);
            return redirect()->route('basement');
        } else {
            echo '<script>alert("Rien ne se passe...")</script>';
            return redirect()->back();
        }
    }
}
