<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Riddle2Controller extends Controller
{
    public function diplay()
    {
        return view('riddles/riddle2');
    }

    public function answer(Request $request)
    {
        $request->validate([
            'answer' => 'required',
        ]);

        if ($request->input('answer') == 'nuages') {
            session(['answer2' => '1']);
            return redirect()->route('garden');
        }else{
            echo '<script>alert("Rien ne se passe...")</script>';
            return redirect()->back();
        }
    }
}
