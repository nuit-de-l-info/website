<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ZoneController extends Controller
{
    public function livingroom()
    {
        return view('livingroom');
    }

    public function bathroom()
    {
        return view('bathroom');
    }

    public function bedroom()
    {
        return view('bedroom');
    }

    public function garden()
    {
        return view('garden');
    }

    public function basement()
    {
        return view('basement');
    }

    public function kitchen()
    {
        return view('kitchen');
    }
}
