<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class GameController extends Controller
{
    public function createUser(Request $request)
    {
        $request->validate([
            'pseudo' => 'required',
        ]);

        //Create user
        $user = new User();
        $user->pseudo = $request->input('pseudo');
        $user->save();

        session('answer1', '0');
        session('answer2', '0');
        session('answer3', '0');
        session('answer4', '0');
        session('answer5', '0');

        Auth::login($user);
        Cookie::queue('jsToken', 'test', '25', null, null, false, false);
        return redirect()->route('livingroom');
    }
}
