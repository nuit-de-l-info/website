<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Riddle3Controller extends Controller
{
    public function diplay()
    {
        return view('riddles/riddle3');
    }

    public function answer(Request $request)
    {
        $request->validate([
            'answer' => 'required',
        ]);

        if ($request->input('answer') == 'eau') {
            session(['answer3' => '1']);
            return redirect()->route('bathroom');
        }else{
            echo '<script>alert("Rien ne se passe...")</script>';
            return redirect()->back();
        }
    }
}
