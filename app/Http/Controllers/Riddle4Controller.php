<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Riddle4Controller extends Controller
{
    public function diplay()
    {
        return view('riddles/riddle4');
    }

    public function answer(Request $request)
    {
        $request->validate([
            'answer' => 'required',
        ]);

        if ($request->input('answer') == 'robot') {
            session(['answer4' => '1']);
            return redirect()->route('kitchen');
        } else {
            echo '<script>alert("Rien ne se passe...")</script>';
            return redirect()->back();
        }
    }
}
