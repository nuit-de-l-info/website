<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Riddle1Controller extends Controller
{
    public function diplay()
    {
        return view('riddles/riddle1');
    }

    public function answer(Request $request)
    {
        $request->validate([
            'answer' => 'required',
        ]);

        if ($request->input('answer') == 'chat') {
            echo '<script>alert("Un flash ! Mais oui, votre fidèle compagnon Michel le chat !")</script>';
            session(['answer1' => '1']);
            return redirect()->route('livingroom');
        }else{
            echo '<script>alert("Rien ne se passe...")</script>';
            return redirect()->back();
        }
    }
}
