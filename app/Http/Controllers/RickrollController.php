<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RickrollController extends Controller
{
    public function rickroll()
    {
        return redirect('https://www.youtube.com/watch?v=dQw4w9WgXcQ');
    }
}
