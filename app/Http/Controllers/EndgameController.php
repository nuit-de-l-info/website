<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EndgameController extends Controller
{
    public function loser()
    {
        return view('loser');
    }

    public function winner()
    {
        return view('winner');
    }
}
