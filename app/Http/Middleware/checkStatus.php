<?php


namespace App\Http\Middleware;

use Closure;

class checkStatus
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ( session('answer1') == '1' && session('answer2') == '1' && session('answer3') == '1' && session('answer4') == '1' && session('answer5') == '1'){
            dd('Bravo vous avez gagné, nous n\'avions pas eu le temps de créer toutes nos vues, mais nous esperons que ca vous à plus :)');
        }
        return $next($request);
    }

}
