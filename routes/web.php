<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//----------------Zone section----------------//
//ZoneController: display the zone page
Route::get('/livingroom', 'ZoneController@livingroom')->name('livingroom')->middleware('isAuth', 'checkStatus');
Route::get('/bathroom', 'ZoneController@bathroom')->name('bathroom')->middleware('isAuth', 'checkStatus');;
Route::get('/garden', 'ZoneController@garden')->name('garden')->middleware('isAuth', 'checkStatus');;
Route::get('/basement', 'ZoneController@basement')->name('basement')->middleware('isAuth', 'checkStatus');;
Route::get('/bedroom', 'ZoneController@bedroom')->name('bedroom')->middleware('isAuth', 'checkStatus');;
Route::get('/kitchen', 'ZoneController@kitchen')->name('kitchen')->middleware('isAuth', 'checkStatus');;
//----------------Index section----------------//
//IndexController: display the index page
Route::get('/', 'IndexController@index')->name('index');
//activity store: store elements in our db
Route::post('/', 'GameController@createUser')->name('game.createUser');

//----------------Endgame section----------------//
//EndgameController: display the loser page
Route::get('/loser', 'EndgameController@loser')->name('endgame.loser');
//EndgameController: display the winner page

//----------------Riddle section----------------//
//Riddle1Controller
Route::get('/riddle1', 'Riddle1Controller@diplay')->name('riddle1.display', 'checkStatus');
Route::post('/riddle1', 'Riddle1Controller@answer')->name('riddle1.answer', 'checkStatus');
//Riddle2Controller
Route::get('/riddle2/nuages', 'Riddle2Controller@diplay')->name('riddle2.display', 'checkStatus');
Route::post('/riddle2', 'Riddle2Controller@answer')->name('riddle2.answer', 'checkStatus');
//Riddle3Controller
Route::get('/riddle3', 'Riddle3Controller@diplay')->name('riddle3.display', 'checkStatus');
Route::post('/riddle3', 'Riddle3Controller@answer')->name('riddle3.answer', 'checkStatus');
//Riddle4Controller
Route::get('/riddle4', 'Riddle4Controller@diplay')->name('riddle4.display', 'checkStatus');
Route::post('/riddle4', 'Riddle4Controller@answer')->name('riddle4.answer', 'checkStatus');
//Riddle5Controller
Route::get('/riddle5', 'Riddle5Controller@diplay')->name('riddle5.display', 'checkStatus');
Route::post('/riddle5', 'Riddle5Controller@answer')->name('riddle5.answer', 'checkStatus');

//----------------Rickroll section----------------//
//GetRickRolled
Route::get('/rickrolled', 'RickrollController@rickroll')->name('rickroll');
