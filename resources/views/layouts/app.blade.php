<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- include info from head.blade.php -->
@include('includes.head')
<!--  import value title -->
    <title>EscapeWeb - @yield('title')</title>
<!-- Miaou miaou miaou
      /\ ___ /\
     (  o   o  )
      \  >#<  /
      /       \
     /         \       ^
    |           |     //
     \         /    //
      ///  ///   --
-->
</head>

<body>
@yield('content')
</body>
@include('includes.scripts')
</html>
