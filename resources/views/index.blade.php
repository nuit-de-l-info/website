@extends('layouts.app')
@section('title', 'Accueil')
@section('content')

    <div class="w3-top">
        <div class="w3-bar w3-white w3-card" id="myNavbar">
            <a href="" class="w3-bar-item w3-button w3-wide">EscapeWeb</a>
            <!-- Right-sided navbar links -->
            <div class="w3-right w3-hide-small">
                <a href="#about" class="w3-bar-item w3-button"></a>
                <a href="#work" class="w3-bar-item w3-button"><i class="fa fa-th"></i> Scoreboard</a>
                <a href="#team" class="w3-bar-item w3-button"><i class="fa fa-user"></i> L'équipe de dev</a>
                <a href="#pricing" class="w3-bar-item w3-button"><i class="fa fa-info"></i> A propos</a>
            </div>
        </div>
    </div>

    <!-- Header with full-height image -->
    <header class="bgimg-1 w3-display-container w3-grayscale-min" id="home">

        <div class="w3-display-left w3-text-white" style="padding:48px">
            <span class="w3-jumbo w3-hide-small">EscapeWeb</span><br>
            <span class="w3-xxlarge w3-hide-large w3-hide-medium">EscapeWeb</span><br>
            <span class="w3-large">Prêt à relever le défi ?</span><br>
            <span class="w3-small">Vous aurez 25 minutes pour relever notre défi.</span><br>
            <span class="w3-small">Ecoutez le récap' juste ici.</span><br>
            <audio controls>
                <source src="/assets/audio/intro.mp3" type="audio/mpeg">
                Your browser does not support the audio element.
            </audio><br><br>

            <form method="POST" action="{{route('game.createUser')}}" enctype="multipart/form-data" >
                {{csrf_field()}}
                <div class="form-group">
                    <input type="text" class="form-control" name="pseudo" placeholder="pseudo"/>
                </div>
                <button type="submit" class="btn btn-primary">C'est Parti !</button>
            </form>



        </div>
        <div class="w3-display-bottomleft w3-text-grey w3-large" style="padding:24px 48px">
            <a href="https://www.facebook.com/adel.louarrani"><i class="fa fa-facebook-official w3-hover-opacity"></i></a>
            <a href="https://www.instagram.com/adel_lrr/"><i class="fa fa-instagram w3-hover-opacity"></i></a>
            <a href="https://www.linkedin.com/in/adellrr/"><i class="fa fa-linkedin w3-hover-opacity"></i></a>
        </div>
    </header>
@endsection
