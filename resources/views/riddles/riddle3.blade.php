@extends('layouts.app')
@section('title', 'Accueil')
@section('content')

    <p>Vous vous souvenez très bien de ce miroir !</p><br>
    <p>Il vous ramène dans votre enfance, vous vous souvenez des minutes passés devant celui.</p><br>
    <p>A vous contempler ou bien seulement à tuer le temps.</p><br>
    <p>Comme quand vous dessiniez dessus grâce à la vapeur d'eau emplissant la salle</p><br>
    <p>On vous reprochait souvent de laisser des traces de doigts, qui réapparaissaient à chaque brouillard.</p><br>
    <p>Peut être pourriez vous retrouver un indice en faisant réapparaître un mot dessus...</p><br>

    <div id="blocImage" style="width: 600px; position: relative">
        <h1 style="position: absolute; top: 8rem; bottom: 0; left: 5rem; right: 0; color: transparent;">eau</h1>
        <img src="/assets/backgrounds/mirror.jpg"/>

    </div>


    <form method="POST" action="{{route('riddle3.answer')}}" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group">
            <input type="text" class="form-control" name="answer" placeholder="réponse"/>
        </div>
        <button type="submit" class="btn btn-primary">Avez vous la bonne réponse ?</button>
    </form>

@include('includes.verifyToken')
@endsection
