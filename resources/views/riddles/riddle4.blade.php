@extends('layouts.app')
@section('title', 'Accueil')
@section('content')

    <p>Un micro-onde...</p><br>
    <p>Comme vous cette machine semble bloqué dans un état second</p><br>
    <p>Sur cette machine se trouve une suite de caractère bizarre : cm9ib3Q=</p><br>
    <p>Peut être que les petits plats pourrons vous aider à retrouver la mémoire...</p><br>
    <p>A noter que le thermostat est reglé sur <strong>64</strong>...</p><br>


    <form method="POST" action="{{route('riddle4.answer')}}" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group">
            <input type="text" class="form-control" name="answer" placeholder="réponse"/>
        </div>
        <button type="submit" class="btn btn-primary">Avez vous la bonne réponse ?</button>
    </form>

    @include('includes.verifyToken')
@endsection
