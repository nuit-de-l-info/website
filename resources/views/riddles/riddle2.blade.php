@extends('layouts.app')
@section('title', 'Accueil')
@section('content')

    <p>Ce ciel est si bleu...</p><br>
    <p>Vous avez ce sentiment de déjà vu...</p><br>
    <p>Vous savez très bien qu'avant votre perte de mémoire vous leviez les yeux en direction de ce ciel.</p><br>
    <p>Mais que regardiez vous ?</p><br>
    <p>Et si vous essayez de vous replonger dans ce ciel et de regarder <strong>en haut</strong> ?</p><br>
    <p>La réponse se trouve peut être la dessus, qui sait, votre mémoire pourrait revenir avec ce détail.</p><br>

    <form method="POST" action="{{route('riddle2.answer')}}" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group">
            <input type="text" class="form-control" name="answer" placeholder="réponse"/>
        </div>
        <button type="submit" class="btn btn-primary">Avez vous la bonne réponse ?</button>
    </form>

    @include('includes.verifyToken')
@endsection
