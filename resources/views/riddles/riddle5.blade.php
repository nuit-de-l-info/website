@extends('layouts.app')
@section('title', 'Accueil')
@section('content')

    <p>Des bruits derrière la porte !</p><br>
    <p>Des coups qui se repètent, la personne derrière peut être utile et vous révéler qui vous êtes !</p><br>
    <p>Mais cet endroit semble sinistre...</p><br>
    <p>C'est trop inquiètant, vous devriez peut etre comprendre qui est derriere avant de vouloir ouvrir.</p><br>

    <audio controls>
        <source src="/assets/audio/morse.mp3" type="audio/mpeg">
        Your browser does not support the audio element.
    </audio><br><br>

    <form method="POST" action="{{route('riddle5.answer')}}" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group">
            <input type="text" class="form-control" name="answer" placeholder="réponse"/>
        </div>
        <button type="submit" class="btn btn-primary">Avez vous la bonne réponse ?</button>
    </form>

    <form method="GET" action="{{route('rickroll')}}" enctype="multipart/form-data">
        {{csrf_field()}}
        <button type="submit" class="btn btn-danger">Ouvrir la porte...</button>
    </form>


    @include('includes.verifyToken')
@endsection
