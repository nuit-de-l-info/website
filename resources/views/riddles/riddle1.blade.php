@extends('layouts.app')
@section('title', 'Accueil')
@section('content')

<p>Vos souvenirs sont flous, pourtant ce magnifique canapé vous fait comme un petit quelque chose.</p><br>
<p>Un sentiment de bonheur vous empli en le regardant, en vous approchant vous remarquez dessus de fins poils le recouvrant.</p><br>
<p>Un animal à pour habitude de se reposer ici ! Surement votre animal de compagnie !</p><br>
<p>Il pourrait être un élement important dans votre quête de réponses</p><br>
<p>Si seulement vous vous rappeliez au moins de quelle espèce animal il s'agissait...</p><br>
<p>Malgré votre perte de mémoire, certains passage de vos documentaires animaliers restent encrés dans votre cerveau.</p><br>
<p>Vous savez très bien que pour retrouver une espèce animale il suffit <strong>d'inspecter le code source</strong> de l'ADN se retrouvant dans les poils sur ce canapé</p><br>
<p>Ou bien existe-t-il une méthode plus simple ?</p><br>

    <form method="POST" action="{{route('riddle1.answer')}}" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group">
            <input type="text" class="form-control" name="answer" placeholder="réponse"/>
        </div>
        <button type="submit" class="btn btn-primary">Avez vous la bonne réponse ?</button>
    </form>

@include('includes.verifyToken')
@endsection
